﻿using UnityEngine;

    public static class GameObjectExtensions {
        
        /// <summary>
        /// Shortcut for setting value for the gameObject.transform.position
        /// </summary>
        /// <param name="pos"></param>
        public static void SetPosition(this GameObject go,Vector3 pos) {
            go.transform.position = pos;
        }

        /// <summary>
        /// shortcut to return gameObject.transform.position
        /// </summary>
        public static Vector3 GetPosition(this GameObject go) {
            return go.transform.position;
        }
        
        /// <summary>
        /// Returns component of type T from gameObject.
        /// If gameObject does not have that component, will add one and return it 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static T Get<T>(this GameObject go) where T : Component {
            var component = go.GetComponent<T>();
            if (!component) component = go.AddComponent<T>();
            return component;
        }

        /// <summary>
        /// Loops through GameObject's children and returns a child with given tag.
        /// Else returns null and logs a warning message 
        /// </summary>
        /// <param name="tag"></param>
        /// <returns></returns>
        public static GameObject FindChildGameObjectWithTag(this GameObject go,string tag) {
            var parent = go.transform;
            foreach (Transform child in parent) {
                if (child.gameObject.CompareTag(tag)) return child.gameObject;
            }
            Debug.LogWarning($"Could not find child gameObject with a tag: ' {tag} ' in {go}");
            return null;
        }
        /// <summary>
        /// Adds child gameObject that is introduced as a parameter
        /// for this Gameobject
        /// </summary>
        /// <param name="go"></param>
        /// <param name="child"></param>
        public static void AddChildGameObject(this GameObject go, GameObject child) {
            if (!child) {
                Debug.LogError("Child to add cannot be null!");
                return;
            }
            
            child.transform.parent = go.transform;
        }
        
        /// <summary>
        /// Loops through gameObject's child gameObjects
        /// and destroys the one with the tag introduced as a parameter
        /// </summary>
        /// <param name="go"></param>
        /// <param name="tag"></param>
        public static void DestroyChildGameObjectWithTag(this GameObject go, string tag) {
            var parent = go.transform;
            foreach (Transform child in parent) {
                if (!child.gameObject.CompareTag(tag)) continue;
                if (Application.isPlaying) {
                    GameObject.Destroy(child.gameObject);                    
                } else {
                    GameObject.DestroyImmediate(child.gameObject);
                }
                return;
            }
        }
        
        /// <summary>
        /// Shortcut for returning gameObject's parent
        /// </summary>
        /// <param name="go"></param>
        /// <returns></returns>
        public static GameObject GetParent(this GameObject go) {
            return go.transform.parent.gameObject;
        }
        
        /// <summary>
        /// Shortcut for setting up parent for gameObject
        /// that is introduced as a parameter 
        /// </summary>
        /// <param name="go"></param>
        /// <param name="parent"></param>
        public static void SetParent(this GameObject go, GameObject parent) {
            go.transform.parent = parent.transform;
        }
    }
