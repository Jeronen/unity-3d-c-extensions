using UnityEngine;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace Tests.Editor {
    public class GameObjectExtensionTests {
        private GameObject go;
        private const string tag = "Player";
        
        [SetUp]
        public void Init() {
            go = new GameObject();
            var child = new GameObject {tag = tag};
            go.AddChildGameObject(child);
        }

        [Test]
        public void ShouldReturnSameGameObjectPosition() {
            Assert.That(go.transform.position.Equals(go.GetPosition()),
                "Shortcut for the transform.position does not match with the original");
        }
        
        [Test]
        public void ShouldChangeGameObjectPosition() {
            var newPosition = new Vector3(10,10,10);
            go.SetPosition(newPosition);
            Assert.That(go.transform.position.Equals(newPosition));
        }

        [Test]
        public void ShouldGetTheComponentFromGameObject() {
            var rb = go.Get<Rigidbody>();
            Assert.IsNotNull(rb,
                "Could not get the component with Get<T> extension that should add the missing component if it is not found");
        }

        [Test]
        public void ShouldFindChildGameObjectWithTheTag() {

            var gameObjectWithTag = go.FindChildGameObjectWithTag(tag);
            Assert.IsNotNull(gameObjectWithTag,"Could not find child gameObject");
            Assert.That(gameObjectWithTag.tag.Equals(tag),"Returned gameObject with wrong tag");
        }

        [Test]
        public void ShouldDestroyChildGameObjectWithTheTag() {
           var gameObjectToDestroy = go.FindChildGameObjectWithTag(tag);
           Assert.IsNotNull(gameObjectToDestroy, "Could not find child gameObject to be destroyed");             
           go.DestroyChildGameObjectWithTag(tag);
           Assert.IsNull(go.FindChildGameObjectWithTag(tag),
               "Should not find child gameObject with tag since it was destroyed"); 
        }

        [Test]
        public void ShouldSetParentForTheGameObject() {
            var parent = new GameObject("parent");
            var newChild = new GameObject("child");
            newChild.SetParent(parent);
            Assert.That(newChild.transform.parent.Equals(newChild.GetParent().transform),
                "Could not set parent for a gameObject");
        }
    }
}